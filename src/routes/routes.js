import React, { memo } from "react"
import { Switch, Route } from "react-router-dom"

// components
import PageNotFound from "../components/404/pageNotFoundContainer"
import HomeContainer from "../components/home/homeContainer"
/**
 * Applicatin routes
 * @returns {JSX.Element}
 * @constructor
 */
const Routes = () => {
	return (
		<>
			<Switch>
				<Route exact path="/" component={HomeContainer} />
				<Route path="*" exact={true} component={PageNotFound} />
			</Switch>
		</>
	)
}

export default memo(Routes)
