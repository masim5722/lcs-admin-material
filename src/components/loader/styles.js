import { makeStyles } from "@material-ui/core"

export default makeStyles(theme => ({
	backdrop: {
		zIndex: theme.zIndex.modal + 1,
		color: "#555",
		background: "#ffffffcf"
	}
}))
