import React from "react"
import { Backdrop, Button, CircularProgress, Grid } from "@material-ui/core"

// styles
import useStyles from "./styles"

/**
 * Presentational component to show loading page
 * @param {*} props
 * @returns
 */
const Loader = ({ isLoading, isPending }) => {
	let classes = useStyles()

	const reload = () => {
		window.location.reload()
	}
	return (
		<Backdrop className={classes.backdrop} open={isLoading}>
			<Grid container style={{ textAlign: "center" }}>
				<Grid lg={12}>
					<CircularProgress color="inherit" />
				</Grid>
				{isPending && (
					<Grid lg={12}>
						<Button
							type="button"
							size="small"
							color={"secondory"}
							variant="contained"
							onClick={reload}
						>
							Reload
						</Button>
					</Grid>
				)}
			</Grid>
		</Backdrop>
	)
}

export default Loader
