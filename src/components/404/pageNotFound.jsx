import { Card, CardContent, Grid } from "@material-ui/core"
import React from "react"
import BreadCrumbs from "../../staticComponents/breadcrumbs/breadcrumbs"
import PageTitle from "../../staticComponents/pageTitle/pageTitle"

// styles
import useStyles from "./../../staticComponents/commonStyles"

/**
 * Presentational Component to show 404 Page
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
const PageNotFound = ({ breadcrumbs }) => {
	const classes = useStyles()

	return (
		<Grid container className={classes.padding}>
			<Grid lg={12} className="mt-0">
				<BreadCrumbs breadcrumbs={breadcrumbs} />
			</Grid>
			<Card className={classes.card}>
				<CardContent>
					<Grid lg={12} className="mt-0">
						<PageTitle title="404" />
					</Grid>
					<Grid lg={12}>
						<h2>The page you looking for does not exist!</h2>
					</Grid>
				</CardContent>
			</Card>
		</Grid>
	)
}

export default PageNotFound
