import React, { useEffect } from "react"
import { useDispatch } from "react-redux"

// components
import PageNotFound from "./pageNotFound"

/**
 * Container Component to show Unauthorized Page
 * @returns
 */
const PageNotFoundContainer = () => {
	// redux related
	const dispatch = useDispatch()

	// component did mount
	useEffect(() => {
		document.title = "404 - Page not Found"
	}, [])

	// breadcrumbs
	const breadcrumbs = [
		{
			id: 0,
			label: "uConfig",
			link: "/uconfig/tenants/list"
		},
		{
			id: 1,
			label: "404-Page not Found",
			link: "/404"
		}
	]

	return <PageNotFound breadcrumbs={breadcrumbs} />
}

export default PageNotFoundContainer
