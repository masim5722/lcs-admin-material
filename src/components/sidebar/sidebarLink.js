import React, { useState, memo } from "react"
import {
	Collapse,
	Divider,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Typography
} from "@material-ui/core"
import { ExpandLess as ExpandLessIcon, ExpandMore as ExpandMoreIcon } from "@material-ui/icons"
import classnames from "classnames"

// styles
import useStyles from "./styles"

// components
import Dot from "./dot"

const SidebarLink = ({
	link,
	icon,
	label,
	children,
	location,
	isSidebarOpened,
	nested,
	type,
	redirectHandler,
	Componento,
	componentProps,
	defaultCollapse,
	rootDisplaySuperInSidebar,
	disable
}) => {
	const handleCollapseClick = e => {
		console.log("asd")
		e.stopPropagation()
		if (isSidebarOpened) {
			e.preventDefault()
			setIsOpen(!isOpen)
		}
	}

	const handleCollapseListClick = (e, link) => {
		redirectHandler(link)
	}

	const ListItemToHandler = link => {
		redirectHandler(link)
	}
	let classes = useStyles()
	// local
	// let [isOpen, setIsOpen] = useState(false)
	let [isOpen, setIsOpen] = useState(defaultCollapse ? defaultCollapse : false)

	let [showAll, setShowAll] = useState(false) // this is for the sidebar items which have child
	let isLinkActive = link && location.pathname === link

	if (type === "title")
		return (
			<Typography
				className={classnames(classes.linkText, classes.sectionTitle, {
					[classes.linkTextHidden]: !isSidebarOpened
				})}
			>
				{label}
			</Typography>
		)

	if (type === "divider") return <Divider className={classes.divider} />
	if (type === "component")
		return <Componento {...componentProps} isSidebarOpened={isSidebarOpened} />

	if (link && link.includes("http")) {
		return (
			<ListItem
				key={label}
				button
				className={classes.link}
				classes={{
					root: classnames(classes.linkRoot, {
						[classes.linkActive]: isLinkActive
					})
				}}
				disableRipple
			>
				<a target="blank" className={classes.externalLink} href={link}>
					<ListItemIcon
						className={classnames(classes.linkIcon, {
							[classes.linkIconActive]: isLinkActive
						})}
					>
						{nested ? <Dot color={isLinkActive && "primary"} /> : icon}
					</ListItemIcon>
					<ListItemText
						classes={{
							primary: classnames(classes.linkText, {
								[classes.linkTextActive]: isLinkActive,
								[classes.linkTextHidden]: !isSidebarOpened
							})
						}}
						title={label}
						primary={label}
					/>
				</a>
			</ListItem>
		)
	}
	if (!children)
		return (
			<ListItem
				button
				key={label}
				onClick={() => {
					ListItemToHandler(link)
				}}
				className={classes.link}
				classes={{
					root: classnames(classes.linkRoot, {
						[classes.linkActive]: isLinkActive
					})
				}}
				disableRipple
				disabled={disable}
				test-data={"sidebar-list-item"}
			>
				<ListItemIcon
					className={classnames(classes.linkIcon, {
						[classes.linkIconActive]: isLinkActive
					})}
				>
					{nested ? <Dot color={isLinkActive && "primary"} /> : icon}
				</ListItemIcon>
				<ListItemText
					classes={{
						primary: classnames(classes.linkText, {
							[classes.linkTextActive]: isLinkActive,
							[classes.linkTextHidden]: !isSidebarOpened
						})
					}}
					title={label}
					primary={
						rootDisplaySuperInSidebar !== undefined && rootDisplaySuperInSidebar ? (
							<>
								{label}
								<sup>Root</sup>
							</>
						) : (
								<>{label}</>
						)
					}
				/>
			</ListItem>
		)

	return (
		<>
			<ListItem
				button
				onClick={e => {
					handleCollapseListClick(e, link)
				}}
				className={classes.link}
				classes={{
					root: classnames(classes.linkRoot, {
						[classes.linkActive]: isLinkActive
					})
				}}
				disableRipple
			>
				<>
					<ListItemIcon
						className={classnames(classes.linkIcon, {
							[classes.linkIconActive]: isLinkActive
						})}
						onClick={e => {
							handleCollapseClick(e)
						}}
						test-data={"collapse-icon"}
					>
						{icon ? icon : isOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}
					</ListItemIcon>
					<ListItemText
						classes={{
							primary: classnames(classes.linkText, {
								[classes.linkTextActive]: isLinkActive,
								[classes.linkTextHidden]: !isSidebarOpened
							})
						}}
						title={label}
						primary={label}
					/>
				</>
			</ListItem>
			{children && (
				<Collapse
					in={isOpen && isSidebarOpened}
					timeout="auto"
					unmountOnExit
					className={classes.nestedList}
				>
					<List component="div" disablePadding>
						{children.map(
							(childrenLink, index) =>
								(showAll ||
									index < 5 ||
									!childrenLink.levelName ) && (
									<SidebarLink
										key={childrenLink && childrenLink.link}
										location={location}
										isSidebarOpened={isSidebarOpened}
										classes={classes}
										redirectHandler={redirectHandler}
										nested
										{...childrenLink}
									/>
								)
						)}
						{children.length > 5 &&
							!showAll &&
							children[0].levelName && (
								<a
									className={classes.showMore}
									onClick={() => {
										setShowAll(true)
									}}
								>
									{" "}
									Show More ({label})
								</a>
							)}
					</List>
				</Collapse>
			)}
		</>
	)
}

export default memo(SidebarLink)
