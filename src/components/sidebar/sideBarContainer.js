import {
	home
} from "./../../constants/sideBarConstants"

// default sidebar items
const sideBarItems = {
	home
}

// function to return sidebar items according to the url
export const getSideBarItems = sidebar => {
	return sideBarItems.hasOwnProperty(sidebar)
		? sideBarItems[sidebar]
		: sideBarItems["defaultItems"]
}
