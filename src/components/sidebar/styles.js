import { makeStyles } from "@material-ui/styles"

const drawerWidth = 265

export default makeStyles(theme => ({
	menuButton: {
		marginLeft: 12,
		marginRight: 36
	},
	hide: {
		display: "none"
	},
	marginTop20: {
		marginTop: "20px"
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
		whiteSpace: "nowrap",
		background: "#69b2db"
	},
	drawerOpen: {
		width: drawerWidth,
		background: "#69b2db",
		transition: theme.transitions.create("width", {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	listItem: {
		background: "#00508F"
	},
	drawerClose: {
		background: "#69b2db",
		transition: theme.transitions.create("width", {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		}),
		overflowX: "hidden",
		width: theme.spacing(7) + 40,
		[theme.breakpoints.down("sm")]: {
			width: drawerWidth
		}
	},
	toolbar: {
		...theme.mixins.toolbar,
		[theme.breakpoints.down("sm")]: {
			display: "none"
		}
	},
	content: {
		flexGrow: 1,
		padding: "24px !important"
	},
	link: {
		paddingLeft: "0 !important",
		textDecoration: "none",

		"&:focus": {
			backgroundColor: "#69b2db"
		},
		"&:hover": {
			backgroundColor: theme.palette.background.sideBar
		}
	},
	externalLink: {
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		textDecoration: "none"
	},
	linkActive: {
		"&:hover, &:focus": {
			backgroundColor: theme.palette.background.sideBar
		},
		backgroundColor: theme.palette.background.sideBar
	},
	linkNested: {
		paddingLeft: 0,
		"&:hover, &:focus": {
			backgroundColor: theme.palette.background.sideBar
		}
	},
	linkIcon: {
		color: "white !important",
		transition: theme.transitions.create("color"),
		width: 24,
		display: "flex",
		justifyContent: "center",
		"& > div": {
			backgroundColor: "white !important"
		}
	},
	linkIconActive: {
		color: "white !important",
		"& > div": {
			backgroundColor: "white !important"
		}
	},
	linkText: {
		padding: 0,
		color: theme.palette.background.light,
		transition: theme.transitions.create(["opacity", "color"]),
		fontSize: 16
	},
	linkTextActive: {
		color: theme.palette.background.light
	},
	linkTextHidden: {
		opacity: 0
	},
	nestedList: {
		paddingLeft: "20px !important"
	},
	sectionTitle: {
		marginLeft: "16px !important",
		marginTop: "16px !important",
		marginBottom: "16px !important"
	},
	divider: {
		marginTop: "16px !important",
		marginBottom: "16px !important",
		height: 1,
		backgroundColor: "#D8D8D880"
	},
	/* sidebarList: {
    marginTop: theme.spacing(6),
  }, */
	mobileBackButton: {
		marginTop: theme.spacing(0.5),
		marginLeft: 18,
		[theme.breakpoints.only("sm")]: {
			marginTop: theme.spacing(0.625)
		},
		[theme.breakpoints.up("md")]: {
			display: "none"
		}
	},
	showMore: {
		color: "white !important",
		cursor: "pointer"
	},
	showVersion: {
		position: "fixed",
		bottom: "10px",
		left: "70px",
		marginTop: "0px !important",
		paddingTop: "0px !important",
		color: "white",
		fontSize: 13
	}
}))
