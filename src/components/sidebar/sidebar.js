import React, { useState, useEffect, memo } from "react"
import { Drawer, IconButton, List, Typography } from "@material-ui/core"
import { ArrowBack as ArrowBackIcon } from "@material-ui/icons"
import { withRouter } from "react-router-dom"
import classNames from "classnames"
import { Scrollbars } from "react-custom-scrollbars"

// styles
import useStyles from "./styles"
import {
	Apps as AppsIcon
} from "@material-ui/icons"
// components
import SidebarLink from "./sidebarLink"
import { isEmpty } from "lodash"

//Tooltip
import Tooltip from "@material-ui/core/Tooltip"


/**
 * Sidebar Component
 * @param {*} param0
 * @returns
 */
const Sidebar = ({ location, history }) => {
	let classes = useStyles()

	// redux related
	const structure = [
		{
			id: 0,
			label: "Home",
			link: '/',
			icon: <AppsIcon />
		}
	]
	const isSidebarOpened =true

	let [isPermanent, setPermanent] = useState(true)

	const [redirectTo, setRedirectTo] = useState("")

	return (
		<Drawer
			variant={isPermanent ? "permanent" : "temporary"}
			className={classNames(classes.drawer, {
				[classes.drawerOpen]: isSidebarOpened,
				[classes.drawerClose]: !isSidebarOpened
			})}
			classes={{
				paper: classNames({
					[classes.drawerOpen]: isSidebarOpened,
					[classes.drawerClose]: !isSidebarOpened
				})
			}}
			open={isSidebarOpened}
		>
			<Scrollbars
				autoHeight
				autoHide
				autoHeightMin={100}
				autoHeightMax={"90vh"}
				style={{ width: "100wh" }}
				renderTrackHorizontal={props => (
					<div {...props} style={{ display: "none" }} className="track-horizontal" />
				)}
			>
				<div className={classes.toolbar} />
				<div className={classes.mobileBackButton}>
					<IconButton
						test-data={"toggle-sidebar-icon"}
					>
						<ArrowBackIcon
							classes={{
								root: classNames(classes.headerIcon, classes.headerIconCollapse)
							}}
						/>
					</IconButton>
				</div>
				<List className={classes.sidebarList}>
					{structure &&
						structure.map((link, index) =>
							isEmpty(link.tooltipMessage) ? (
								<SidebarLink
									className={classes.listItem}
									key={link.id + "" + index}
									location={location}
									isSidebarOpened={isSidebarOpened}
									{...link}
									Componento={link.component}
									componentProps={link.componentProps}
								/>
							) : (
								<Tooltip title={link.tooltipMessage}>
									<div>
										<SidebarLink
											className={classes.listItem}
											key={link.id + "" + index}
											location={location}
											isSidebarOpened={isSidebarOpened}
											{...link}
											Componento={link.component}
											componentProps={link.componentProps}
											setRedirectTo={setRedirectTo}
										/>
									</div>
								</Tooltip>
							)
						)}
				</List>
			</Scrollbars>
			
		</Drawer>
	)
}

export default memo(withRouter(Sidebar))
