import { makeStyles } from "@material-ui/styles"

export default makeStyles(theme => ({
	root: {
		display: "flex",
		maxWidth: "100vw",
		overflowX: "hidden"
	},
	content: {
		flexGrow: 1,
		// padding: '24px !important',
		width: "calc(100vw - 265px)",
		height: "calc(100vh - 55px) !important",
		overflow: "hidden !important",
		marginTop: "55px",
		float: "right"
	},
	marginTop75: {
		marginTop: "86px !important"
	},
	contentShift: {
		width: "calc(100vw - 96px)",
		transition: theme.transitions.create(["width", "margin"], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	fakeToolbar: {
		...theme.mixins.toolbar
	},
	link: {
		"&:not(:first-child)": {
			paddingLeft: 15
		}
	}
}))
