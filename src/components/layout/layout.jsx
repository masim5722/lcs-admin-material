import React, { useEffect, memo } from "react"
import { BrowserRouter as Router } from "react-router-dom"
import classnames from "classnames"
import "react-notifications/lib/notifications.css"
// styles
import useStyles from "./style"

// components
import Header from "../header/header"
import Sidebar from "../sidebar/sidebar"
import Routes from "../../routes/routes"
import Scrollbars from "react-custom-scrollbars"
import { Grid } from "@material-ui/core"

/**
 * Layout Component
 * @returns {JSX.Element}
 * @constructor
 */
const Layout = () => {
	let classes = useStyles()
	const isSidebarOpened = true

	return (
		<>
			<Router>
				<Grid container className={classes.layoutContainer}>
					<Header />
					<Sidebar />
					<div
						className={classnames(classes.content, {
							[classes.contentShift]: !isSidebarOpened
						})}
					>
						<Scrollbars
							autoHeight
							autoHide
							autoHeightMin={100}
							autoHeightMax={"90vh"}
							style={{ width: "100wh" }}
						>
							{/* Application routes */}
							<Routes />
						</Scrollbars>
					</div>
				</Grid>
			</Router>
		</>
	)
}

export default memo(Layout)
