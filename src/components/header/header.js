import React, { useState, memo } from "react"
import { useDispatch, useSelector } from "react-redux"
import {
	AppBar,
	Toolbar,
	IconButton,
	Menu,
	MenuItem,
	Typography,
	Badge,
	Divider
} from "@material-ui/core"
import {
	Menu as MenuIcon,
	NotificationsNone as NotificationsIcon,
	Person as AccountIcon,
	ExitToApp as LogoutIcon
} from "@material-ui/icons"
import { withRouter } from "react-router"
import SettingsIcon from "@material-ui/icons/Settings"
import classNames from "classnames"
// styles
import useStyles from "./styles"


/**
 * Presentational component to show header
 * @param {*} props
 * @returns
 */
const Header = props => {
	let classes = useStyles()

	let [profileMenu, setProfileMenu] = useState(null)

	const handleSidebar = () => {
		
	}

	const handleSetProfile = e => {
		setProfileMenu(e.currentTarget)
	}

	return (
		<AppBar position="fixed" className={classes.appBar}>
			{/* Tool bar */}
			<Toolbar className={classes.toolbar}>
				<IconButton
					color="inherit"
					test-data="menu-icon-button"
					onClick={handleSidebar}
					className={classNames(
						classes.headerMenuButtonSandwich,
						classes.headerMenuButtonCollapse
					)}
				>
					<MenuIcon
						classes={{
							root: classNames(classes.headerIcon, classes.headerIconCollapse)
						}}
					/>
				</IconButton>

				<Typography variant="h6" weight="medium" className={classes.logotype}>
					LCS Admin
				</Typography>
				<div className={classes.grow} />
				<>
					<IconButton
						color="inherit"
						aria-haspopup="true"
						test-data="notification-icon-button"
						aria-controls="mail-menu"
						disabled={true}
					>
						<Badge badgeContent={null} color="secondary">
							<NotificationsIcon classes={{ root: classes.headerIcon }} />
						</Badge>
					</IconButton>
					<IconButton
						aria-haspopup="true"
						color="inherit"
						test-data="user-icon-button"
						className={classes.headerMenuButton}
						aria-controls="profile-menu"
						onClick={handleSetProfile.bind(this)}
					>
						<AccountIcon classes={{ root: classes.headerIcon }} />
					</IconButton>
					<Menu
						id="notifications-menu"
						className={classes.headerMenu}
						disableAutoFocusItem
					></Menu>
					<Menu
						id="profile-menu"
						open={Boolean(profileMenu)}
						anchorEl={profileMenu}
						onClose={() => setProfileMenu(null)}
						className={classes.headerMenu}
						classes={{ paper: classes.profileMenu }}
						test-data="profile-menu"
						disableAutoFocusItem
					>
						<div className={classes.profileMenuUser}>
							<Typography variant="h4" weight="medium">
								John Doe
							</Typography>
						</div>
						<MenuItem
							test-data="user-profile"
							className={classNames(
								classes.profileMenuItem,
								classes.headerMenuItem
							)}
						>
							<AccountIcon className={classes.profileMenuIcon} /> Profile
						</MenuItem>

						<MenuItem
							test-data="user-settings"
							className={classNames(
								classes.profileMenuItem,
								classes.headerMenuItem
							)}
						>
							<SettingsIcon className={classes.profileMenuIcon} /> Settings
						</MenuItem>

						<Divider className={classes.divider} />

						<MenuItem
							inputProps={{ "test-data": "logout-user" }}
							test-data="logout-user"
							className={classNames(
								classes.profileMenuItem,
								classes.headerMenuItem
							)}
						>
							<LogoutIcon className={classes.profileMenuIcon} /> Logout
						</MenuItem>
					</Menu>
				</>
			</Toolbar>
		</AppBar>
	)
}

export default memo(withRouter(Header))
