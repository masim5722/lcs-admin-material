import { makeStyles } from "@material-ui/styles"
import { alpha } from "@material-ui/core/styles"

export default makeStyles(theme => ({
	logo: {
		height: "40px !important",
		paddingTop: "8px",
		cursor: "pointer"
	},
	websiteLabel: {
		position: "absolute",
		top: "14px",
		marginLeft: "20px"
	},
	logotype: {
		color: "white",
		marginLeft: theme.spacing(2.5),
		marginRight: theme.spacing(2.5),
		fontWeight: 500,
		fontSize: 18,
		whiteSpace: "nowrap",
		[theme.breakpoints.down("xs")]: {
			display: "none"
		}
	},
	divider: {
		marginTop: "5px",
		marginBottom: "5px"
	},
	appBar: {
		background: "transparent !important",
		width: "100vw",
		zIndex: theme.zIndex.modal - 1,
		transition: theme.transitions.create(["margin"], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		})
	},
	notice: {
		padding: "5px 10px !important",
		fontSize: "12px",
		margin: "2px",
		backgroundColor: "#5f1717"
	},
	noticeClose: {
		fontSize: "14px",
		fontWeight: "bold",
		cursor: "pointer",
		color: "#ffffff",
		padding: "0 !important"
	},
	noticeCloseContainer: {
		textAlign: "right"
	},
	noticeContainer: {
		backgroundColor: "#f6f7ff"
	},
	toolbar: {
		backgroundColor: "#00508F",
		paddingLeft: "16px !important",
		paddingRight: "16px !important",
		minHeight: "55px !important"
	},
	hide: {
		display: "none"
	},
	grow: {
		flexGrow: 1
	},
	search: {
		position: "relative",
		borderRadius: 25,
		paddingLeft: theme.spacing(2.5),
		width: 36,
		backgroundColor: alpha(theme.palette.common.black, 0),
		transition: theme.transitions.create(["background-color", "width"]),
		"&:hover": {
			cursor: "pointer",
			backgroundColor: alpha(theme.palette.common.black, 0.08)
		}
	},
	searchFocused: {
		backgroundColor: alpha(theme.palette.common.black, 0.08),
		width: "100%",
		[theme.breakpoints.up("md")]: {
			width: 250
		}
	},
	searchIcon: {
		width: 36,
		right: 0,
		height: "100%",
		position: "absolute",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		transition: theme.transitions.create("right"),
		"&:hover": {
			cursor: "pointer"
		}
	},
	searchIconOpened: {
		right: theme.spacing(1.25)
	},
	inputRoot: {
		color: "inherit",
		width: "100%"
	},
	inputInput: {
		height: 36,
		padding: 0,
		paddingRight: 36 + theme.spacing(1.25),
		width: "100%"
	},
	messageContent: {
		display: "flex",
		flexDirection: "column"
	},
	headerMenu: {
		marginTop: "50px !important"
	},
	headerMenuList: {
		display: "flex",
		flexDirection: "column"
	},
	headerMenuItem: {
		"&:hover, &:focus": {
			backgroundColor: theme.palette.background.light
			// color: "white",
		}
	},
	headerMenuButton: {
		marginLeft: "16px !important",
		padding: theme.spacing(0.5)
	},
	headerMenuButtonSandwich: {
		marginLeft: 9,
		[theme.breakpoints.down("sm")]: {
			marginLeft: 0
		},
		padding: theme.spacing(0.5)
	},
	headerMenuButtonCollapse: {
		marginRight: "16px !important"
	},
	headerIcon: {
		fontSize: 28,
		color: "rgba(255, 255, 255, 0.35)"
	},
	headerIconCollapse: {
		color: "white"
	},
	profileMenu: {
		minWidth: 265
	},
	profileMenuUser: {
		display: "flex",
		flexDirection: "column",
		padding: "16px !important"
	},
	profileMenuItem: {
		color: "#383737"
	},
	profileMenuIcon: {
		marginRight: "16px !important",
		color: "#383737",
		"&:hover": {
			color: theme.palette.primary.main
		}
	},
	profileMenuLink: {
		fontSize: 16,
		textDecoration: "none",
		"&:hover": {
			cursor: "pointer"
		}
	},
	messageNotification: {
		height: "auto",
		display: "flex",
		alignItems: "center",
		"&:hover, &:focus": {
			backgroundColor: theme.palette.background.light
		}
	},
	messageNotificationSide: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		marginRight: "16px !important"
	},
	messageNotificationBodySide: {
		alignItems: "flex-start",
		marginRight: 0
	},
	sendMessageButton: {
		margin: "32px !important",
		marginTop: "16px !important",
		marginBottom: "16px !important",
		textTransform: "none"
	},
	sendButtonIcon: {
		marginLeft: "16px !important"
	},
	purchaseBtn: {
		[theme.breakpoints.down("sm")]: {
			display: "none"
		},
		marginRight: "24px !important"
	}
}))
