import React, { useEffect } from "react"
import { useDispatch } from "react-redux"

// components
import Home from "./home"

/**
 * Container Component to show Home Page
 * @returns
 */
const HomeContainer = () => {
	// component did mount
	useEffect(() => {
		document.title = "Home"
	}, [])

	// breadcrumbs
	const breadcrumbs = [
		{
			id: 0,
			label: "Home",
			link: "/home"
		}
	]

	return <Home breadcrumbs={breadcrumbs} />
}

export default HomeContainer
