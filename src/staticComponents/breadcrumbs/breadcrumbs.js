import React, { useState } from "react"
import Breadcrumbs from "@material-ui/core/Breadcrumbs"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import { withRouter } from "react-router-dom"
import Tooltip from "@material-ui/core/Tooltip"

// styles
import useStyles from "./styles"
/**
 * Presentational component to show breadcrumbs
 * @param {*} param0
 * @returns
 */
const BreadCrumbs = ({ breadcrumbs, history }) => {
	const classes = useStyles()

	let breadCrumbsWithLinks = []
	for (let i = 0; i < breadcrumbs.length - 1; i++) {
		breadCrumbsWithLinks.push(breadcrumbs[i])
	}
	const lastBreadcrumb = breadcrumbs[breadcrumbs.length - 1]

	return (
		<div className={classes.root}>
			<Breadcrumbs maxItems={10} separator="›" aria-label="breadcrumb">
				{breadCrumbsWithLinks.map(breadcrumb => (
					<Tooltip title={breadcrumb.label} key={breadcrumb.id}>
						<Link
							tooltip={breadcrumb.label}
							style={{ cursor: "pointer" }}
							onClick={() => {
								history.push(breadcrumb.link)
							}}
							color="inherit"
						>
							{breadcrumb.label}
						</Link>
					</Tooltip>
				))}
				<Typography color="textPrimary">{lastBreadcrumb.label}</Typography>
			</Breadcrumbs>
		</div>
	)
}

export default withRouter(BreadCrumbs)
