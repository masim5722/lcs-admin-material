import { makeStyles } from "@material-ui/core"

export default makeStyles({
	padding: {
		padding: "15px"
	},
	primary: {
		background: "#00508F",
		padding: "6px 5%",
		fontSize: "0.9rem !important",
		color: "white",
		marginLeft: "8px",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #69b2db"
		}
	},
	card: {
		minHeight: "calc(85vh - 55px) !important",
		marginBottom: "50px",
		paddingBottom: "15px",
		width: "100%"
	},
	"@keyframes blinker": {
		from: { opacity: 1 },
		to: { opacity: 0 }
	},
	blink: {
		animationName: "$blinker",
		animationDuration: "1s",
		animationTimingFunction: "linear",
		animationIterationCount: "infinite"
	},
	root: {
		marginTop: "16px !important",
		"& > *": {
			margin: "16px !important",
			width: "100%",
			marginLeft: "2px"
		}
	},
	card1: {
		marginTop: "8px !important",
		minHeight: "calc(85vh - 55px) !important",
		// width: `calc(100vw - 280px)`,
		width: "100% !important",
		marginBottom: "50px",
		paddingBottom: "15px"
	},
	title: {
		fontSize: 14
	},
	listRoot: {
		background: "#00508f",
		color: "#eee"
	},
	media: {
		height: 140
	},
	icon: {
		fontSize: "3em"
	},
	p4: {
		padding: "1.3rem"
	},
	p3: {
		padding: "0.5rem"
	},
	f07: {
		fontSize: "0.7rem",
		marginRight: "5px"
	},
	mt5: {
		marginTop: "5rem"
	},
	mr20: {
		marginRight: "20px"
	},
	pl20: {
		paddingLeft: "20px"
	},
	mt4: {
		marginTop: "4rem"
	},
	mt3: {
		marginTop: "3rem"
	},
	deleteColor: {
		color: "red",
		cursor: "pointer"
	}
})
