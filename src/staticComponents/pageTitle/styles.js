import { makeStyles } from "@material-ui/styles"

export default makeStyles(theme => ({
	pageTitleContainer: {
		display: "flex",
		justifyContent: "space-between",
		marginBottom: "8px !important",
		marginTop: "8px !important"
	},
	typo: {
		color: theme.palette.text.hint
	},
	button: {
		boxShadow: theme.customShadows.widget,
		textTransform: "none",
		"&:active": {
			boxShadow: theme.customShadows.widgetWide
		}
	}
}))
