import { Typography } from "@material-ui/core"
import React from "react"

// styles
import useStyles from "./styles"

/**
 * Reuseable Presentational Component to show Page Title
 * @param {*} props
 * @returns
 */
const PageTitle = props => {
	let classes = useStyles()

	return (
		<div className={classes.pageTitleContainer} style={props.divStyle}>
			<Typography className={classes.typo} variant="h2" size="sm">
				{props.title}
			</Typography>
			{props.button && <>{props.button}</>}
		</div>
	)
}
export default PageTitle
