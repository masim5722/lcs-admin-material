import React from "react"
import PropTypes from "prop-types"
import { CircularProgress } from "@material-ui/core"
import { DialogContent, DialogTitle } from "@material-ui/core"
import Dialog from "@material-ui/core/Dialog"

const SimpleDialog = props => {
	const { onClose, message, open } = props

	return (
		<Dialog
			onClose={(event, reason) => {
				if (reason !== "backdropClick") {
					onClose(event, reason)
				}
			}}
			aria-labelledby="simple-dialog-title"
			open={open}
		>
			<DialogTitle id="simple-dialog-title">{message}</DialogTitle>
			<DialogContent align={"center"}>
				<CircularProgress />
			</DialogContent>
		</Dialog>
	)
}

SimpleDialog.propTypes = {
	onClose: PropTypes.func.isRequired,
	open: PropTypes.bool.isRequired
}

const DialogWithLoader = ({ message, isOpen }) => {
	const [open, setOpen] = React.useState(isOpen)

	const handleClose = () => {
		setOpen(!open)
	}

	return (
		<div>
			<SimpleDialog message={message} open={isOpen} onClose={handleClose} />
		</div>
	)
}

export default DialogWithLoader
