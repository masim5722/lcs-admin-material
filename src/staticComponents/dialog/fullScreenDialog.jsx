import React from "react"
import {
	Button,
	Dialog,
	DialogActions,
	DialogTitle,
	DialogContent,
	DialogContentText
} from "@material-ui/core"
import ListingTable from "../../components/release/ListingTable"

const FullScreenDialog = ({ isOpen, title, proceedBtn, cancelBtn, tableData }) => {
	return (
		<div>
			<Dialog
				open={isOpen}
				onClose={(event, reason) => {
					if (reason !== "backdropClick") {
						cancelBtn.onClick()
					}
				}}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogTitle id="alert-dialog-title">{title}</DialogTitle>
				<DialogContent style={{ width: "595px" }}>
					<DialogContentText id="alert-dialog-description">
						<ListingTable stock={tableData} className={tableData.className} />
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={cancelBtn.onClick} color="primary">
						{cancelBtn.text}
					</Button>
					<Button onClick={proceedBtn.onClick} color="primary">
						{proceedBtn.text}
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	)
}

export default FullScreenDialog
