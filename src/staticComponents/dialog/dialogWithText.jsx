import React from "react"
import {
	Button,
	Dialog,
	DialogActions,
	DialogTitle,
	DialogContent,
	DialogContentText
} from "@material-ui/core"

const DialogWithText = ({ isOpen, title, proceedBtn, cancelBtn, message }) => {
	return (
		<div>
			<Dialog
				open={isOpen}
				onClose={(event, reason) => {
					if (reason !== "backdropClick") {
						cancelBtn.onClick()
					}
				}}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogTitle id="alert-dialog-title">{title}</DialogTitle>

				{message && (
					<DialogContent>
						<DialogContentText>{message}</DialogContentText>
					</DialogContent>
				)}

				<DialogActions>
					<Button
						test-data={"dialog-with-text-cancel"}
						onClick={cancelBtn.onClick}
						color="primary"
					>
						{cancelBtn.text}
					</Button>
					<Button
						test-data={"dialog-with-text-proceed"}
						onClick={proceedBtn.onClick}
						color="primary"
					>
						{proceedBtn.text}
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	)
}

export default DialogWithText
