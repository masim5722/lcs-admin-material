import React from "react"
import { Grid } from "@material-ui/core"
import { useEffect } from "react"

const ErrorFallback = ({ error, resetErrorBoundary }) => {
	useEffect(() => {
		resetErrorBoundary()
	}, [error])
	return <Grid>{error && <span>Invalid Json</span>}</Grid>
}

export default ErrorFallback
