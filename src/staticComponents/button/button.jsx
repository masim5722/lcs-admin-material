import { Button } from "@material-ui/core"
import classnames from "classnames"
import React from "react"

// styles
import useStyles from "./styles"

const CustomButton = ({ label, type, onclick, icon, disable }) => {
	let classes = useStyles()

	return (
		<>
			<Button
				onClick={onclick}
				disabled={disable}
				className={
					disable
						? classnames(classes.disabled)
						: classnames(classes.buttoon, {
								[classes.success]: type === "success",
								[classes.successMediumSize]: type === "successMediumSize",
								[classes.error]: type === "error",
								[classes.errorMediumSize]: type === "errorMediumSize",
								[classes.warning]: type === "warning",
								[classes.warningMediumSize]: type === "warningMediumSize",
								[classes.primary]: type === "primary",
								[classes.save]: type === "save",
								[classes.failedMediumSize]: type === "failedMediumSize"
						  })
				}
			>
				{icon} {label}
			</Button>
		</>
	)
}

export default CustomButton
