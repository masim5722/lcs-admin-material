import { makeStyles } from "@material-ui/core"

export default makeStyles({
	primary: {
		background: "#00508F",
		color: "white",
		marginLeft: "8px",
		fontSize: "0.7rem !important",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #69b2db"
		}
	},
	warning: {
		background: "#e79631",
		color: "white",
		marginLeft: "8px",
		fontSize: "0.7rem !important",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #edb166"
		}
	},
	warningMediumSize: {
		background: "#e79631",
		color: "white",
		marginLeft: "8px",
		fontSize: "0.9rem !important",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #edb166"
		}
	},
	error: {
		background: "#ce320e",
		color: "white",
		marginLeft: "8px",
		fontSize: "0.7rem !important",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #f1694a"
		}
	},
	errorMediumSize: {
		background: "#ce320e",
		color: "white",
		marginLeft: "8px",
		fontSize: "0.9rem !important",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #f1694a"
		}
	},
	failedMediumSize: {
		background: "#00508F",
		color: "white",
		marginLeft: "8px",
		fontSize: "0.9rem !important",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #69b2db"
		}
	},
	success: {
		background: "#2ea72e",
		color: "white",
		marginLeft: "8px",
		fontSize: "0.7rem !important",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #37c837"
		}
	},
	successMediumSize: {
		background: "#2ea72e",
		color: "white",
		marginLeft: "8px",
		fontSize: "0.9rem !important",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #37c837"
		}
	},
	button: {
		background: "#2ea72e",
		color: "white",
		marginLeft: "8px",
		fontSize: "0.7rem !important",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #37c837"
		}
	},
	save: {
		background: "#00508F",
		padding: "6px 5%",
		fontSize: "0.9rem !important",
		color: "white",
		marginLeft: "8px",
		textTransform: "capitalize",
		"&:hover, &:focus": {
			background: " #69b2db"
		}
	},
	disabled: {
		background: "#cccccc",
		color: "#666666",
		padding: "6px 5%",
		fontSize: "0.9rem !important",
		marginLeft: "8px",
		textTransform: "capitalize"
	}
})
