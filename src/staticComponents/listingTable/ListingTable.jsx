import React from "react"
import { DataGrid } from "@mui/x-data-grid"
import { useState } from "react"

/**
 * Release List table component
 * @param {*} props
 * @returns
 */
const ListingTable = ({ data, columns, className }) => {
	// using for pagination
	const [pageSize, setPageSize] = useState(25)

	return (
		<div className={className}>
			<DataGrid
				rows={data}
				columns={columns}
				disableColumnSelector={true}
				pageSize={pageSize}
				rowsPerPageOptions={[25, 50, 100]}
				pagination
				onPageSizeChange={newPageSize => {
					setPageSize(newPageSize)
				}}
			/>
		</div>
	)
}

export default ListingTable
