import React, { memo } from "react"
import { Tooltip } from "@material-ui/core"

import useStyles from "./styles"
import { isEmpty } from "lodash"
import { limitChar } from "../../helpers/functions"

/**
 * Tenant name component for listing table
 * @param {*} param0
 * @returns
 */
const ShowTooltip = ({ title, maxLength = 0 }) => {
	const classes = useStyles()
	if (!isEmpty(title)) {
		return (
			<>
				{title.length > maxLength ? (
					<Tooltip title={title}>
						<span className={classes.clickableColumn}>
							{limitChar(title, maxLength)}...
						</span>
					</Tooltip>
				) : (
					<span> {title}</span>
				)}
			</>
		)
	} else {
		return <span />
	}
}

export default memo(ShowTooltip)
