import React from "react"
import ReactDOM from "react-dom"
import reportWebVitals from "./reportWebVitals"
import { ThemeProvider } from "@material-ui/core/styles"
import theme from "./configs/themes/theme"
import { Provider } from "react-redux"
import { CssBaseline } from "@material-ui/core"
import { CookiesProvider } from "react-cookie"
import Layout from "./components/layout/layout"
import "typeface-roboto"

ReactDOM.render(
	<React.StrictMode>
		<ThemeProvider theme={theme.default}>
			<CssBaseline />
			<CookiesProvider>
				<Layout />
			</CookiesProvider>
		</ThemeProvider>
	</React.StrictMode>,
	document.getElementById("root")
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
